import csv_handler
import csv

def test_read():
    assert csv_handler.read('test1.csv')[2][1]=='4'

def test_clean_data():
    c = csv_handler
    assert  c.clean_data(c.read('test1.csv'))[2][1]==4

def test_clean_data2():
    c = csv_handler
    assert  c.clean_data(c.read('test1.csv'))[2][1]!='4'


def test_read_write():
    data = [[0,"zero"],[1,"one"],[2,"two"]]
    filename = "test_rw.csv"
    c = csv_handler
    c.write(data,filename,'w')
    assert data == c.clean_data(c.read(filename))
