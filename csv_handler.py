import csv


def read(file):
    out = []
    with open(file,'r') as f:
        reader = csv.reader(f)
        for line in reader:
            print(line)
            out.append(line)
    return out
def write(data,file,mode):
    with open(file,mode) as f:
        writer = csv.writer(f)
        writer.writerows(data)


def clean_data(data):
    for i,d in enumerate(data):
        for j,_ in enumerate(d):
            try:
                data[i][j]=int(data[i][j])+10
            except Exception:
                pass
    return data
